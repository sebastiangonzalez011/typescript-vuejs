import { VuexModule, Module, Mutation, Action } from "vuex-module-decorators";
import { iRadioButtons, iDollarRate, iStock } from "@/store/models/inputFormTypes";
import store from "@/store";

@Module({
  namespaced: true,
  dynamic: true,
  store,
  name: "inputState",
})
export default class InputStore extends VuexModule {
  public stock: iStock = {
    id: 9345384753487543,
    type: "Something",
    description: "Some description about something",
    price: 10,
    stock: 100
  }
  public radioButtons: iRadioButtons[] = [
    {
      id: "none",
      label: "None",
      disable: false,
      fields: [
        { id: "ratio", type: "number", label: "Size Ratio", disable: true },
        { id: "shares", type: "number", label: "Shares", disable: false },
        { id: "notional", type: "number", label: "Notional", disable: false },
      ]
    },
    {
      id: "sizeratio",
      label: "Size Ratio",
      disable: false,
      fields: [
        { id: "ratio", type: "number", label: "Size Ratio", disable: false },
        { id: "shares", type: "number", label: "Shares", disable: false },
        { id: "notional", type: "number", label: "Notional", disable: true },
      ]
    },
    {
      id: "dollarneutral",
      label: "Dollar Neutral",
      disable: false,
      fields: [
        { id: "ratio", type: "number", label: "Size Ratio", disable: true },
        { id: "shares", type: "number", label: "Shares", disable: true },
        { id: "notional", type: "number", label: "Notional", disable: false },
      ]
    },
    {
      id: "cash",
      label: "Cash",
      disable: false,
      fields: [
        { id: "ratio", type: "number", label: "Size Ratio", disable: false },
        { id: "shares", type: "number", label: "Shares", disable: false },
        { id: "notional", type: "number", label: "Notional", disable: false },
      ]
    },
  ];
  public inputsModel: any = {
    ratio: 0,
    shares: 0,
    notional: 0
  }
  public radioButtonSelected: iRadioButtons = this.radioButtons[0]
  public dollarRate?: iDollarRate;

  get getStock(): iStock {
    return this.stock;
  }

  get inputModel() {
    return this.inputsModel;
  }
  @Mutation
  setInputModel(newModel:any) {
    this.inputsModel = newModel
  }

  @Mutation
  setNewValueInModel(newValue: any) {
    const { id, value } = newValue
    this.inputsModel[id] = value
  }

  @Mutation
  setDollarRate(newDollarRate: iDollarRate) {
    this.dollarRate = newDollarRate;
  }

  @Mutation
  setRadioButtonSelected(newSelected: iRadioButtons) {
    this.radioButtonSelected = newSelected;
  }

  @Action({commit: 'setDollarRate'})
  async updateDollarRate() {
    const response = await fetch("https://api-dolar-argentina.herokuapp.com/api/dolarpromedio");
    if (response.ok) {
      const res: iDollarRate = await response.json();
      return res;
    } else {
      console.error(response.text);
      return null
    }
  }
}