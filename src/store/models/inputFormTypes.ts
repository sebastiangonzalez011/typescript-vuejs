export interface iDollarRate {
  fecha: string,
  compra: string,
  venta: string,
}

export interface iRadioButtons {
  id: string,
  label: string,
  disable: boolean,
  fields: iInputs[],
}

export interface iInputs {
  id: string,
  type: string,
  label: string,
  disable: boolean,
}

export interface iStock {
  id: number,
  type: string,
  description: string,
  price: number,
  stock: number
}